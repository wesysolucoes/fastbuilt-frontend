import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { withRouter } from "react-router-dom";

import LogoFile from '../../assets/Logo-Black.png';
import LoginImg from '../../assets/Login-Image.png';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import api from '../../services/api';


const Login = () => {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [logged, setLogged] = useState<boolean>(false);

  const logging = () => {
    api.post('/login', {
      "email": email,
      "password": password,
      "remember_me": false
    }).then((res) => {
      localStorage.setItem(
        "authorization",
        "Bearer " + res.data.access_token
      );
      setLogged(true);
    });
  }

  const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
  }

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(e.target.value);
  }

  if (logged) return <Redirect to={"/enterprise"} push/>;

  return (
    <Container style={styles.container} fluid>
      <Row>
        <Col style={styles.rightCol} className="text-center px-4 py-5">
          <img src={LogoFile} alt="Fastbuilt"/>
          <h3 style={styles.subtitle}>Acesse a sua conta</h3>
          <form className="pt-3 ">
            <div style={styles.fieldDiv}>
              <label>Email</label>
              <input onChange={handleEmailChange} style={styles.input} type="text" placeholder="examplo@email.com"/>
            </div>
            <div>
              <label>Senha</label>
              <input onChange={handlePasswordChange} style={styles.input} type="password"/>
              <span style={styles.forgotPass}>Esqueceu a sua senha?</span>
            </div>
            <button type="button" onClick={() => logging()} style={styles.button} className="mt-5">ENTRAR</button>
          </form>
        </Col>
      </Row>
    </Container>
  );
}

const styles = {
  fieldDiv: {
  },
  fieldItem: {
    'alignSelf': 'flexStart'
  },
  forgotPass: {
  },
  button: {
    'background': '#3279CD',
    'borderRadius': '2px',
    'color': '#FAFAFA',
    'width': '100%',
    'fontSize': '14px',
    'border': 'none',
    'height': '6vh'
  },
  input: {
    'width': '100%',
    'height': '6vh',
    'background': '#F2F2F2',
    'border': 'none',
    'padding': '1rem'
  },
  container: {
    'background': '#FAFAFA',
    'height': '100vh'
  },
  subtitle: {
    'fontStyle': 'normal',
    'fontWeight': 500,
    'fontSize': '22px',
    'lineHeight': '22px',
    'color': '#081630',
    'marginTop': '15vh',
  },
  rightCol: {
    'maxWidth': '750px',
  }
};


export default withRouter(Login);
//<Image className="lg-7 p-0 d-none-sm" src={LoginImg} rounded/>
//<img className="img-fluid" src={LoginImg} alt=""/>