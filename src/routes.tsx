import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import PrivateRoute from './components/private-route';
import Login from './pages/login';
import Enterprise from './pages/enterprise';

const Routes = () => {
  return (
      <BrowserRouter>
        <Route exact path="/login" component={Login} />
        <PrivateRoute  path="/enterprise"  component={Enterprise}  exact  />
      </BrowserRouter>
  );
}


export default Routes;

