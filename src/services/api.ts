import axios from 'axios';

const api = axios.create({
  baseURL: "http://fastbuild.test/api",
  timeout: 10000,
  headers: {
    authorization: localStorage.getItem("authorization"),
  },
});

export default api;