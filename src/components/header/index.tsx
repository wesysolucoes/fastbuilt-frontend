import React, { useState, useEffect } from 'react';
import api from '../../services/api';

const Header = () => {
  const [firstName, setFirstName] = useState<string>("");

  useEffect(() => {
    api.get('/user/auth').then(response => {
      setFirstName(response.data.user.first_name);
    });
  }, []);

  return (
    <div style={styles.container}>
      <div style={styles.title}>Olá, { firstName }</div>
    </div>
  );
}

const styles = {
  container: {
    'display': 'flex'
  },
  title: {
    'alignSelf': 'flexEnd'
  }
};


export default Header;