import  React from  "react";
import { Route, Redirect } from  "react-router-dom";
import { isAuth } from '../../services/auth';

const  PrivateRoute: React.FC<{
        component: React.FC;
        path: string;
        exact: boolean;
    }> = (props) => {

    const condition = isAuth();

    return  condition ? (<Route  path={props.path}  exact={props.exact} component={props.component} />) : 
        (<Redirect  to="/page/login"  />);
};
export  default  PrivateRoute;